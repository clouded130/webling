﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Webling.Interfaces;
using Webling.Models;


namespace Webling.Controllers
{
   
    public class HomeController : Controller
    {

        private readonly IFileImportService _fileImportService;

        public HomeController(IFileImportService fileImportService)
        {
            _fileImportService = fileImportService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ViewCustomers(IFormFile file)
        {
            var CustomerList = _fileImportService.ReadFromExcel(file); 
            return View(CustomerList);
        }


    }

}
