﻿using System;
using System.Collections.Generic;
using System.Linq;
using Webling.Models;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Webling.Interfaces;

public class FileImportService : IFileImportService
{
     private IHostingEnvironment _hostingEnvironment;

     public FileImportService(IHostingEnvironment hostingEnvironment)
     {
        _hostingEnvironment = hostingEnvironment;
     }

        public IEnumerable<Customer> ReadFromExcel(IFormFile file) {
            var CustomerList = new List<Customer>();
            var InvoiceList = new List<Invoice>();
            var InvoiceItemsList = new List<InvoiceItem>();
            string folderName = "Upload";
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName);
            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            if (file.Length > 0)
            {
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();

                ISheet Customers;
                ISheet CustomersAddress;
                ISheet Invoice;
                ISheet InvoiceItems;

               

                string fullPath = Path.Combine(newPath, file.FileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Position = 0;

                    if (sFileExtension == ".xls")
                    {
                        //This will read the Excel 97-2000 formats
                        HSSFWorkbook hssfwb = new HSSFWorkbook(stream);
                    //get sheets from workbook  
                        Customers = hssfwb.GetSheetAt(1);
                        CustomersAddress = hssfwb.GetSheetAt(2);
                        Invoice = hssfwb.GetSheetAt(3);
                        InvoiceItems = hssfwb.GetSheetAt(4);
                    }
                    else
                    {
                        //This will read 2007 Excel format
                        XSSFWorkbook hssfwb = new XSSFWorkbook(stream);
                        //get sheets from workbook  
                        Customers = hssfwb.GetSheetAt(1);
                        CustomersAddress = hssfwb.GetSheetAt(2);
                        Invoice = hssfwb.GetSheetAt(3);
                        InvoiceItems = hssfwb.GetSheetAt(4);
                    }

                    //Get Header Row
                    IRow headerRow = Customers.GetRow(0);
                    int cellCount = headerRow.LastCellNum;

                    //Read Excel File
                    for (int i = (Customers.FirstRowNum + 1); i <= Customers.LastRowNum; i++)
                    {
                        IRow row = Customers.GetRow(i);
                        IRow row1 = CustomersAddress.GetRow(i);
                        IRow row2 = Invoice.GetRow(i);
                        IRow row3 = InvoiceItems.GetRow(i);
                        if (row == null) continue;
                        if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                        //Creates  lists 
                        var item = new InvoiceItem(row3.GetCell(0).ToString(), row3.GetCell(1).ToString(), row3.GetCell(2).ToString(), row3.GetCell(3).ToString(), row3.GetCell(4).ToString());
                        InvoiceItemsList.Add(item);
                        var invoice = new Invoice(row2.GetCell(0).ToString(), row2.GetCell(1).ToString(), row2.GetCell(2).ToString(), row2.GetCell(3).ToString(), row2.GetCell(4).ToString());
                        InvoiceList.Add(invoice);
                        var customer = new Customer(row.GetCell(0).ToString(), row.GetCell(1).ToString(), row.GetCell(2).ToString(), row1.GetCell(0).ToString(), row1.GetCell(1).ToString());
                        CustomerList.Add(customer);
                    }
                }

                foreach (var invoice in InvoiceList)
                {
                    foreach (var item in InvoiceItemsList)
                    {
                        if (invoice.InvoiceID == item.InvoiceID)
                        {
                            invoice.Items.Add(item);
                        }
                    }
                }

                foreach (var customer in CustomerList)
                {
                    foreach (var invoice in InvoiceList)
                    {
                        if (customer.CustomerID == invoice.CustomerID)
                        {
                            customer.Invoices.Add(invoice);
                        }
                    }

                }


                foreach (var invoice in InvoiceList)
                {
                    foreach (var product in invoice.Items)
                    {
                        invoice.Total = Convert.ToDouble(invoice.Total) + Convert.ToDouble(product.UnitPrice);
                    }

                }

            }
            return CustomerList;
        }
}
