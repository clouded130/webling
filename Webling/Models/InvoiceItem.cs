﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webling.Models
{
    public class InvoiceItem
    {

        public InvoiceItem(string itemID, string invoiceID, string productName, string quantity, string unitPrice)
        {
            InvoiceItemID = itemID;
            InvoiceID = invoiceID;
            ProductName = productName;
            Quantity = quantity;
            UnitPrice = unitPrice;
        }

        public string InvoiceItemID { get; set; }
        public string InvoiceID { get; set; }
        public string ProductName { get; set; }
        public string Quantity { get; set; }
        public string UnitPrice { get; set; }
    }
}
