﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webling.Models
{
    public class Customer
    {

        public Customer(string customerID, string firstName, string lastName, string  address, string mobile)
        {
            CustomerID = customerID;
            FirstName = firstName;
            LastName = lastName;
            Address = address;
            Mobile = mobile;
            Invoices = new List<Invoice>();
        }

        public string CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Mobile { get; set; }

        public virtual ICollection<Invoice> Invoices { get; set; }

    }
}
