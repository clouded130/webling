﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Webling.Models
{
    public class Invoice
    {

        public Invoice(string invoiceID, string customerID, string note, string dueDate, string createdDate)
        {
            InvoiceID = invoiceID;
            CustomerID = customerID;
            Note = note;
            DueDate = dueDate;
            CreatedDate = createdDate;
            Items = new List<InvoiceItem>();
        }


        public string InvoiceID { get; set; }
        public string CustomerID { get; set; }
        public string Note { get; set; }
        public string DueDate { get; set; }
        public string CreatedDate { get; set; }
        public double Total { get; set; }

        public ICollection<InvoiceItem> Items { get; set; }

    }
}
