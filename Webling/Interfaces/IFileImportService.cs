﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Webling.Models;

namespace Webling.Interfaces
{
    public interface IFileImportService
    {
        IEnumerable<Customer> ReadFromExcel(IFormFile file);
    }
}
